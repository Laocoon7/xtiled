﻿using Citadel.XTiled.Elements;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml.Linq;

namespace Citadel.XTiled.Tools
{
    public static class Extentions
    {
        #region XTiled helpers
        public static List<EEditorSettings> GetEditorSettings(this XTiled xTiled)
        {
            var returnValue = xTiled?.Map?.EditorSettings;
            return returnValue.Count > 0 ? returnValue : null;
        }

        public static EEditorSettings GetFirstEditorSettings(this XTiled xTiled)
        {
            var returnValue = GetEditorSettings(xTiled);
            return returnValue != null ? returnValue[0] : null;
        }

        public static EEditorSettings GetLastEditorSettings(this XTiled xTiled)
        {
            var returnValue = GetEditorSettings(xTiled);
            return returnValue != null ? returnValue[returnValue.Count - 1] : null;
        }
        public static List<ETileset> GetTilesets(this XTiled xTiled)
        {
            var returnValue = xTiled?.Map?.Tilesets;
            return returnValue.Count > 0 ? returnValue : null;
        }

        public static ETileset GetFirstTileset(this XTiled xTiled)
        {
            var returnValue = GetTilesets(xTiled);
            return returnValue != null ? returnValue[0] : null;
        }

        public static ETileset GetLastTileset(this XTiled xTiled)
        {
            var returnValue = GetTilesets(xTiled);
            return returnValue != null ? returnValue[returnValue.Count - 1] : null;
        }
        #region Tileset Helpers
        public static List<Bitmap> GetBitmaps(this ETileset tileset)
        {
            return null;
        }
        #endregion
        #endregion

        #region TryGetAttribute
        /// <summary>
        /// Looks up a string value from an XElement-XAttribute.
        /// </summary>
        /// <param name="element">XElement that may contain the XAttribute</param>
        /// <param name="attributeName">Name of the XAttribute we want the value of.</param>
        /// <param name="defaultValue">Value in returnString if XAttribute doesn't exist.</param>
        /// <param name="returnString">Return Value</param>
        /// <returns>True on successful lookup.</returns>
        public static bool TryGetAttribute (this XElement element, string attributeName, string defaultValue, out string returnString)
        {
            var value = element.Attribute(attributeName);

            if (value == null)
            {
                returnString = defaultValue;
                return false;
            }

            returnString = value.Value;
            return true;
        }

        /// <summary>
        /// Looks up an int value from an XElement-XAttribute.
        /// </summary>
        /// <param name="element">XElement that may contain the XAttribute</param>
        /// <param name="attributeName">Name of the XAttribute we want the value of.</param>
        /// <param name="defaultValue">Value in returnInt if XAttribute doesn't exist / isn't an int.</param>
        /// <param name="returnInt">Return Value</param>
        /// <returns>True on successful lookup.</returns>
        public static bool TryGetAttribute(this XElement element, string attributeName, int defaultValue, out int returnInt)
        {
            var value = element.Attribute(attributeName);

            if (value == null || String.IsNullOrWhiteSpace(value.Value))
            {
                returnInt = defaultValue;
                return false;
            }

            if (int.TryParse(value.Value, out returnInt))
                return true;

            returnInt = defaultValue;
            return false;
        }

        /// <summary>
        /// Looks up a float value from an XElement-XAttribute.
        /// </summary>
        /// <param name="element">XElement that may contain the XAttribute</param>
        /// <param name="attributeName">Name of the XAttribute we want the value of.</param>
        /// <param name="defaultValue">Value in returnFloat if XAttribute doesn't exist / isn't a float.</param>
        /// <param name="returnFloat">Return Value</param>
        /// <returns>True on successful lookup.</returns>
        public static bool TryGetAttribute(this XElement element, string attributeName, float defaultValue, out float returnFloat)
        {
            var value = element.Attribute(attributeName);

            if (value == null || String.IsNullOrWhiteSpace(value.Value))
            {
                returnFloat = defaultValue;
                return false;
            }

            if (float.TryParse(value.Value, out returnFloat))
                return true;

            returnFloat = defaultValue;
            return false;
        }

        /// <summary>
        /// Looks up a bool value (1/0/true/false) from an XElement-XAttribute.
        /// </summary>
        /// <param name="element">XElement that may contain the XAttribute</param>
        /// <param name="attributeName">Name of the XAttribute we want the value of.</param>
        /// <param name="defaultValue">Value in returnBool if XAttribute doesn't exist / isn't a bool.</param>
        /// <param name="returnBool">Return Value</param>
        /// <returns>True on successful lookup.</returns>
        public static bool TryGetAttribute(this XElement element, string attributeName, bool defaultValue, out bool returnBool)
        {
            var value = element.Attribute(attributeName);

            if (value == null || String.IsNullOrWhiteSpace(value.Value))
            {
                returnBool = defaultValue;
                return false;
            }

            if (value.Value == "0" || value.Value == "false")
            {
                returnBool = false;
                return true;
            }
            else if (value.Value == "1" || value.Value == "true")
            {
                returnBool = true;
                return true;
            }

            returnBool = defaultValue;
            return false;
        }
        #endregion
    }
}
