﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Citadel.XTiled.Elements;

namespace Citadel.XTiled
{
    /// <summary>
    /// XDocument wrapper for "Tiled" maps
    /// </summary>
    public class XTiled
    {
        // Reference to the actual XDocument
        public XDocument XDocument;

        #region Elements
        public EMap Map
        {
            get
            {
                var map = XDocument.Element(STRINGS.MAP);
                return (map != null) ? new EMap(XDocument.Element(STRINGS.MAP)) : null;
            }
        }
        public EMap CreateMap()
        {
            return new EMap(new XElement(STRINGS.MAP));
        }
        public void AddMap(EMap map)
        {
            if (map == null)
                throw new ArgumentNullException();

            if (Map != null)
                throw new Exception($"A Map already exists on this XTiled.");

            XDocument.Add(map.XElement);
        }
        #endregion

        public void Save(string filePath, bool disableFormatting = false)
        {
            if (disableFormatting)
                XDocument.Save(filePath, SaveOptions.DisableFormatting);
            else
                XDocument.Save(filePath);
        }

        /// <summary>
        /// Creates a new XTiled document instance.
        /// </summary>
        public XTiled()
        {
            XDocument = new XDocument();
        }

        /// <summary>
        /// Creates a new XTiled document instance and loads an existing Tmx file.
        /// </summary>
        /// <param name="tmxFilePath"></param>
        public XTiled(string tmxFilePath)
        {
            XDocument = XDocument.Load(tmxFilePath, LoadOptions.SetBaseUri);
        }

        /// <summary>
        /// Creates a new XTiled document instance and loads an existing Tmx file.
        /// </summary>
        /// <param name="tmxFileStream"></param>
        public XTiled(Stream tmxFileStream)
        {
            XDocument = XDocument.Load(tmxFileStream, LoadOptions.SetBaseUri);
        }

        /// <summary>
        /// Creates a new XTiled document instance and loads an existing Tmx file.
        /// </summary>
        /// <param name="tmxFileReader"></param>
        public XTiled(TextReader tmxFileReader)
        {
            XDocument = XDocument.Load(tmxFileReader, LoadOptions.SetBaseUri);
        }

        /// <summary>
        /// Creates a new XTiled document instance and loads an existing Tmx file.
        /// </summary>
        /// <param name="tmxFileReader"></param>
        public XTiled(XmlReader tmxFileReader)
        {
            XDocument = XDocument.Load(tmxFileReader, LoadOptions.SetBaseUri);
        }

        public override string ToString()
        {
            return XDocument.ToString();
        }
    }

    /*
    <map>
        <editorsettings>
            <chunksize></chunksize>
            <export></export>
        </editorsettings>
        <tileset>
            <tileoffset></tileoffset>
            <grid></grid>
            <image></image>
            <terraintypes>
                <terrain></terrain>
            </terraintypes>
            <tile> <!-- multiple -->
                <animation></animation>
            </tile>
            <wangsets>
                <wangset></wangset>
            </wangsets>
            <properties>
                <property></property>
            <properties>
        </tileset>
        <layer></layer>
    </map>
    <properties>
        <property></property>
    <properties>
    */
}