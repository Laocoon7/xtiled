﻿using Citadel.XTiled.Elements;
using System;
using System.Xml.Linq;

namespace Citadel.XTiled
{
    public class TilesetFile
    {
        XDocument XDocument;

        #region Elements
        public ETileset Tileset
        {
            get
            {
                var tileset = XDocument.Element(STRINGS.TILESET);
                return tileset != null ? new ETileset(tileset) : null;
            }
        }
        public ETileset CreateTileset()
        {
            return new ETileset(new XElement(STRINGS.TILESET));
        }
        public void AddTileset(ETileset tileset)
        {
            if (tileset == null)
                throw new ArgumentNullException();

            if (Tileset != null)
                throw new Exception($"A Tileset already exists on this TilesetFile.");

            XDocument.Add(tileset.XElement);
        }
        #endregion

        public void Save(string filePath, bool disableFormatting = false)
        {
            if (disableFormatting)
                XDocument.Save(filePath, SaveOptions.DisableFormatting);
            else
                XDocument.Save(filePath);
        }

        public TilesetFile()
        {
            XDocument = new XDocument();
        }

        public TilesetFile(string tilesetFilePath)
        {
            XDocument = XDocument.Load(tilesetFilePath, LoadOptions.SetBaseUri);
        }

        public override string ToString()
        {
            return XDocument.ToString();
        }
    }
}
