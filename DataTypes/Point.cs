﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Citadel.XTiled.DataTypes
{
    public class Point
    {
        public int X;
        public int Y;

        public Point()
        {
            X = 0;
            Y = 0;
        }

        public Point (int x, int y)
        {
            X = x;
            Y = y;
        }

        public static List<Point> FromValue(string value)
        {
            var points = new List<Point>();
            var values = value.Trim().Split(' ', ',');
            
            try
            {
                for (int i = 0; i < values.Length; i += 2)
                {
                    var point = new Point(int.Parse(values[i]), int.Parse(values[i + 1]));
                    points.Add(point);
                }
            }
            catch
            {
                throw new Exception($"Value[{value}] does not appear to contain valid Points data.");
            }

            return points;
        }

        public static string ToValue(List<Point> points)
        {
            StringBuilder value = new StringBuilder();

            foreach (var point in points)
            {
                value.Append($"{point.X},{point.Y} ");
            }

            return value.ToString().Trim();
        }
    }
}
