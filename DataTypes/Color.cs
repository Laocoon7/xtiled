﻿using System;

namespace Citadel.XTiled.DataTypes
{
    public class Color
    {
        private int mAlpha;
        public int Alpha { get => mAlpha; set => mAlpha = IsValid(value) ? value : mAlpha; }

        private int mRed;
        public int Red { get => mRed; set => mRed = IsValid(value) ? value : mRed; }

        private int mGreen;
        public int Green { get => mGreen; set => mGreen = IsValid(value) ? value : mGreen; }

        private int mBlue;
        public int Blue { get => mBlue; set => mBlue = IsValid(value) ? value : mBlue; }

        private bool IsValid(int value) => ((value >= 0) && (value <= 255));

        public Color()
        {
            Alpha = 0;
            Red = 0;
            Green = 0;
            Blue = 0;
        }

        public Color(int alpha, int red, int green, int blue)
        {
            Alpha = alpha;
            Red = red;
            Green = green;
            Blue = blue;
        }

        public Color(string color)
        {
            var colorString = color.Trim();
            if (colorString.IndexOf("#") != -1)
                colorString.Replace("#", "");

            if (colorString.Length == 6)
            {
                Alpha = 0;
                Red = int.Parse(colorString.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                Green = int.Parse(colorString.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                Blue = int.Parse(colorString.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
            }
            else if (colorString.Length == 8)
            {
                Alpha = int.Parse(colorString.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                Red = int.Parse(colorString.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                Green = int.Parse(colorString.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                Blue = int.Parse(colorString.Substring(6, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
            }
            else
                throw new Exception($"Color[{color}] does not appear to be a valid color. Proper color strings: '[#][AA]RRGGBB'. Items in square brackets are optional.");
        }

        public override string ToString()
        {
            return String.Format("#{0:X02}{1:X02}{2:X02}{3:X02}", Alpha, Red, Green, Blue);
        }

        public string ToStringNoAlpha()
        {
            return String.Format("#{0:X02}{1:X02}{2:X02}", Red, Green, Blue);
        }
    }
}
