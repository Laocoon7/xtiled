﻿namespace Citadel.XTiled.DataTypes
{
    public enum Orientation
    {
        DoesNotExist,
        Orthogonal,
        Isometric,
        Staggered,
        Hexagonal
    }

    public enum RenderOrder
    {
        DoesNotExist,
        RightDown,
        RightUp,
        LeftDown,
        LeftUp
    }

    public enum StaggerAxis
    {
        DoesNotExist,
        X,
        Y
    }

    public enum StaggerIndex
    {
        DoesNotExist,
        Even,
        Odd
    }

    public enum ObjectAlignment
    {
        DoesNotExist,
        Unspecified,
        TopLeft,
        Top,
        TopRight,
        Left,
        Center,
        Right,
        BottomLeft,
        Bottom,
        BottomRight
    }

    public enum DrawOrder
    {
        DoesNotExist,
        Index,
        TopDown
    }

    public enum HAlign
    {
        DoesNotExist,
        Left,
        Center,
        Right,
        Justify
    }

    public enum VAlign
    {
        DoesNotExist,
        Top,
        Center,
        Bottom
    }
}
