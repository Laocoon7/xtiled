﻿# XTiled #

A C# object oriented library implementation of [Tiled](https://www.mapeditor.org/) map data.

### Usage ###

To start working with a pre-existing TMX file, simply:
```
var TmxFile = new XTiled("Path/To/File.tmx");
```

From here, each XML element beginning with <MAP> is available from the [TMX Map Format](https://doc.mapeditor.org/en/stable/reference/tmx-map-format/#).

### A few design choices I made ###
* All TMX XML elements have their own class derived from the ElementBase class in the form E[TAG]. (Example: EMap is the top level <map> element.) As there are two different forms of Tiles, I split them into ELayerDataTile and ETilesetTile.
* Tilesets can be self contained in their own file (TSX). Creating an instance of 'new TilesetFile("Path/To/File.tsx");' provides an easy way to continue accessing information while also seperating the file into a new base object.
* Templates work the same way as Tilesets and have their own TemplateFile class. However, as they are not automatically loaded, the developer needs to make sure to check and apply Template data before overwritting with Tileset data.
* When multiple XML elements of the same type can be found under an element, I decided to return a List<Element>. These will never return null, and the only way to see if they exist is to check (List<Element>)Elements.Count > 0. This allows for cleaner foreach statements without needing a null check first.
* All Elements do nothing in their constructor besides pass their underlying XElement to ElementBase.
* Elements are responsible for creating and adding their children.
* All XDocument / XElement references are public for extra control by the developer if necessary.
* I used Enums for some string options that should not change.
* Return values: If a value is nullable, and it doesn't exist in the Top XDocument, it returns null. Integers and Floats return -1. Booleans return the default value, and each have their own XXXExists property. Enums return DoesNotExist.
* <Properties><Property Value> (EProperty.Value) will return a type based on <Property Type>(EProperty.Type) defaulting to a string. Use EProperty.GetValue(true) to force the return type to be a string, and EProperty.ValueExists to check existance. Maybe I'll turn this into a custom Union class at some point.. Also don't forget to check EProperty.InnerValue for multi-lined strings.
* EMap.AllLayers and EGroup.AllLayers provide the layers of the map in order of appearance.
* EObject.[Shapes] sometimes refer back to the parent object's attributes. These are currently linked to the parents and will throw an exception if they can't find their parent.

### Examples ###

Load a tmx file, and get and load the first Tileset's source file:
```
var TmxFile = new XTiled("./Sample.tmx");
var tilesets = TmxFile.Map?.Tilesets
if (tilesets.Count > 0)
{
	var TsxFile = new TilesetFile(tilesets[0].Source);
	...
}
```

Creating a new tmx file and saving it:
```
var TmxFile = new XTiled();
try
{
	TmxFile.AddMap(TmxFile.CreateMap());
	TmxFile.Map.Width = 16;
	TmxFile.Map.Height = 16;
	...
	TmxFile.Save("./NewFile.tmx");
}
```

### Notes ###

* I was tired of not having a decent object oriented way of dealing with Tiled maps / resources. So I wrote some wrapper classes using XDocument for tmx/tsx/template files.
* I normally try to optimize everything for speed, but this time I decided to make ease of use the priority.
* EWangTile.WangId currently returns a string. This will be updated to return a UInt32 like it's supposed to along with some other tools in the future.

### Licensing ###

* BSD-2-Clause

### Contact ###

* Wes Feldsine - [Laocoon7@gmail.com](mailto:Laocoon7@gmail.com?subject=TiledX)