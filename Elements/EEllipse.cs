﻿using System;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class EEllipse : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to (EObject)Parent.X
        /// </summary>
        public int X
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).X;
                }
                catch
                {
                    throw new Exception($"This Ellipse does not have a parent object.");
                }
            }

            set
            {
                try
                {
                    new EObject(XElement.Parent).X = value;
                }
                catch
                {
                    throw new Exception($"This Ellipse does not have a parent object.");
                }
            }
        }
        public bool XExists
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).XExists;
                }
                catch
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Defaults to (EObject)Parent.Y
        /// </summary>
        public int Y
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).Y;
                }
                catch
                {
                    throw new Exception($"This Ellipse does not have a parent object.");
                }
            }

            set
            {
                try
                {
                    new EObject(XElement.Parent).Y = value;
                }
                catch
                {
                    throw new Exception($"This Ellipse does not have a parent object.");
                }
            }
        }
        public bool YExists
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).YExists;
                }
                catch
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Defaults to (EObject)Parent.Width
        /// </summary>
        public int Width
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).Width;
                }
                catch
                {
                    throw new Exception($"This Ellipse does not have a parent object.");
                }
            }

            set
            {
                try
                {
                    new EObject(XElement.Parent).Width = value;
                }
                catch
                {
                    throw new Exception($"This Ellipse does not have a parent object.");
                }
            }
        }
        public bool WidthExists
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).WidthExists;
                }
                catch
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Defaults to (EObject)Parent.Height
        /// </summary>
        public int Height
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).Height;
                }
                catch
                {
                    throw new Exception($"This Ellipse does not have a parent object.");
                }
            }

            set
            {
                try
                {
                    new EObject(XElement.Parent).Height = value;
                }
                catch
                {
                    throw new Exception($"This Ellipse does not have a parent object.");
                }
            }
        }
        public bool HeightExists
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).HeightExists;
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion

        #region Elements
        #endregion

        public EEllipse(XElement element) : base(element)
        {
        }
    }
}
