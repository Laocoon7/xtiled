﻿using System;
using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EImageLayer : ELayerBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int X
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.X, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.X, value);
            }
        }
        public bool XExists => XElement.TryGetAttribute(STRINGS.X, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Y
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.Y, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.Y, value);
            }
        }
        public bool YExists => XElement.TryGetAttribute(STRINGS.Y, -1, out int _);
        #endregion

        #region Elements
        public EImage Image
        {
            get
            {
                var image = XElement.Element(STRINGS.IMAGE);
                return image != null ? new EImage(image) : null;
            }
        }
        public EImage CreateData()
        {
            return new EImage(new XElement(STRINGS.IMAGE));
        }
        public void AddImage(EImage image)
        {
            if (image == null)
                throw new ArgumentNullException();

            if (Image != null)
                throw new Exception($"An Image has already been created for this ImageLayer.");

            XElement.Add(image.XElement);
        }
        #endregion

        public EImageLayer(XElement element) : base(element)
        {
        }
    }
}
