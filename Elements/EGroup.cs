﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class EGroup : ELayerBase
    {
        #region Attributes
        #endregion

        #region Elements
        public List<ELayerBase> AllLayers
        {
            get
            {
                var returnValue = new List<ELayerBase>();
                foreach (var layer in XElement.Elements().Where(x => x.Name.LocalName == STRINGS.LAYER || x.Name.LocalName == STRINGS.OBJECTGROUP || x.Name.LocalName == STRINGS.IMAGELAYER || x.Name.LocalName == STRINGS.GROUP))
                {
                    switch (layer.Name.LocalName)
                    {
                        case STRINGS.LAYER:
                            returnValue.Add(new ELayer(layer));
                            break;
                        case STRINGS.OBJECTGROUP:
                            returnValue.Add(new EObjectGroup(layer));
                            break;
                        case STRINGS.IMAGELAYER:
                            returnValue.Add(new EObjectGroup(layer));
                            break;
                        case STRINGS.GROUP:
                            returnValue.Add(new EGroup(layer));
                            break;
                    }
                }

                return returnValue;
            }
        }
        public void AddLayer(ELayerBase layer)
        {
            if (layer == null)
                throw new ArgumentNullException();

            var allLayers = AllLayers;
            if (allLayers.Count > 0)
            {
                AddLayerAfter(layer, allLayers[allLayers.Count - 1]);
                return;
            }
            
            XElement.Add(layer);
        }
        public void AddLayerBefore(ELayerBase layer, ELayerBase existingLayer)
        {
            if (layer == null || existingLayer == null)
                throw new ArgumentNullException();

            existingLayer.XElement.AddBeforeSelf(layer.XElement);
        }
        public void AddLayerAfter(ELayerBase layer, ELayerBase existingLayer)
        {
            if (layer == null || existingLayer == null)
                throw new ArgumentNullException();

            existingLayer.XElement.AddAfterSelf(layer.XElement);
        }

        public List<ELayer> Layers
        {
            get
            {
                var returnValue = new List<ELayer>();
                foreach (var layer in XElement.Elements(STRINGS.LAYER))
                {
                    returnValue.Add(new ELayer(layer));
                }

                return returnValue;
            }
        }
        public ELayer CreateLayer()
        {
            return new ELayer(new XElement(STRINGS.LAYER));
        }

        public List<EObjectGroup> ObjectGroups
        {
            get
            {
                var returnValue = new List<EObjectGroup>();
                foreach (var layer in XElement.Elements(STRINGS.OBJECTGROUP))
                {
                    returnValue.Add(new EObjectGroup(layer));
                }

                return returnValue;
            }
        }
        public EObjectGroup CreateObjectGroup()
        {
            return new EObjectGroup(new XElement(STRINGS.OBJECTGROUP));
        }

        public List<EImageLayer> ImageLayers
        {
            get
            {
                var returnValue = new List<EImageLayer>();
                foreach (var layer in XElement.Elements(STRINGS.IMAGELAYER))
                {
                    returnValue.Add(new EImageLayer(layer));
                }

                return returnValue;
            }
        }
        public EImageLayer CreateImageLayer()
        {
            return new EImageLayer(new XElement(STRINGS.IMAGELAYER));
        }

        public List<EGroup> Groups
        {
            get
            {
                var returnValue = new List<EGroup>();
                foreach (var layer in XElement.Elements(STRINGS.GROUP))
                {
                    returnValue.Add(new EGroup(layer));
                }

                return returnValue;
            }
        }
        public EGroup CreateGroup()
        {
            return new EGroup(new XElement(STRINGS.GROUP));
        }
        #endregion

        public EGroup(XElement element) : base(element)
        {
        }
    }
}
