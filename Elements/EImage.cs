﻿using System;
using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EImage : ElementBase
    {
        #region Attributes
        public string Format
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.FORMAT, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.FORMAT, value);
            }
        }
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Id
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.ID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.ID, value);
            }
        }
        public bool IdExists => XElement.TryGetAttribute(STRINGS.ID, -1, out int _);
        public string Source
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.SOURCE, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.SOURCE, value);
            }
        }
        public Color Trans
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.TRANS, null, out string colorString))
                    return new Color(colorString);

                return null;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TRANS, value.ToStringNoAlpha());
            }
        }
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Width
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WIDTH, value);
            }
        }
        public bool WidthExists => XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Height
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEIGHT, value);
            }
        }
        public bool HeightExists => XElement.TryGetAttribute(STRINGS.HEIGHT, -1, out int _);
        #endregion

        #region Elements
        public EData Data
        {
            get
            {
                var data = XElement.Element(STRINGS.DATA);
                return data != null ? new EData(data) : null;
            }
        }
        public EData CreateData()
        {
            return new EData(new XElement(STRINGS.DATA));
        }
        public void AddData(EData data)
        {
            if (data == null)
                throw new ArgumentNullException();

            if (Data != null)
                throw new Exception($"Data has already been created for this Image.");

            XElement.Add(data.XElement);
        }
        #endregion
        public EImage(XElement element) : base(element)
        {
        }
    }
}
