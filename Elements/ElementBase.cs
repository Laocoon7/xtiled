﻿using System;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    /// <summary>
    /// Base class for any class that represents an XML <tag>
    /// </summary>
    public abstract class ElementBase
    {
        public XElement XElement { get; protected set; }
        public XElement Parent { get => XElement.Parent; }
        public string InnerValue
        {
            get
            {
                return XElement.Value;
            }

            set
            {
                XElement.Value = value;
            }
        }

        public void Remove()
        {
            XElement.Remove();
        }

        public ElementBase(XElement element)
        {
            if (element == null)
                throw new  ArgumentNullException();

            XElement = element;
        }

        public override string ToString()
        {
            return XElement.ToString();
        }
    }
}
