﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EData : ElementBase
    {
        #region Attributes
        public string Encoding
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.ENCODING, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.ENCODING, value);
            }
        }
        public string Compression
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.COMPRESSION, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.COMPRESSION, value);
            }
        }
        #endregion

        #region Elements
        public List<EChunk> Chunks
        {
            get
            {
                var returnValue = new List<EChunk>();
                foreach (var chunk in XElement.Elements(STRINGS.CHUNK))
                {
                    returnValue.Add(new EChunk(chunk));
                }

                return returnValue;
            }
        }
        public EChunk CreateChunk()
        {
            return new EChunk(new XElement(STRINGS.CHUNK));
        }
        public void AddChunk(EChunk chunk)
        {
            if (chunk == null)
                throw new ArgumentNullException();

            var chunks = Chunks;
            if (chunks.Count > 0)
                chunks[chunks.Count - 1].XElement.AddAfterSelf(chunk.XElement);
            else
                XElement.Add(chunk.XElement);
        }

        public List<ELayerDataTile> Tiles
        {
            get
            {
                var returnValue = new List<ELayerDataTile>();
                foreach (var tile in XElement.Elements(STRINGS.TILE))
                {
                    returnValue.Add(new ELayerDataTile(tile));
                }

                return returnValue;
            }
        }
        public ELayerDataTile CreateTile()
        {
            return new ELayerDataTile(new XElement(STRINGS.TILE));
        }
        public void AddTile(ELayerDataTile tile)
        {
            if (tile == null)
                throw new ArgumentNullException();

            var tiles = Tiles;
            if (tiles.Count > 0)
                tiles[tiles.Count - 1].XElement.AddAfterSelf(tile.XElement);
            else
                XElement.Add(tile.XElement);
        }
        #endregion
        public EData(XElement element) : base(element)
        {
        }
    }
}
