﻿using System;
using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class ETilesetTile : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Id
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.ID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.ID, value);
            }
        }
        public bool IdExists => XElement.TryGetAttribute(STRINGS.ID, -1, out int _);
        public string Type
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TYPE, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.TYPE, value);
            }
        }

        // FIXME:
        public string Terrain
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TERRAIN, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.TERRAIN, value);
            }
        }
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Probability
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.PROBABILITY, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.PROBABILITY, value);
            }
        }
        public bool ProbabilityExists => XElement.TryGetAttribute(STRINGS.PROBABILITY, -1, out int _);
        #endregion

        #region Elements
        public EProperties Properties
        {
            get
            {
                var properties = XElement.Element(STRINGS.PROPERTIES);
                return properties != null ? new EProperties(properties) : null;
            }
        }
        public EProperties CreateProperties()
        {
            return new EProperties(new XElement(STRINGS.PROPERTIES));
        }
        public void AddProperties(EProperties properties)
        {
            if (properties == null)
                throw new ArgumentNullException();

            if (Properties != null)
                throw new Exception($"Properties already exists on this Tile.");

            XElement.Add(properties.XElement);
        }

        public EImage Image
        {
            get
            {
                var image = XElement.Element(STRINGS.IMAGE);
                return image != null ? new EImage(image) : null;
            }
        }
        public EImage CreateData()
        {
            return new EImage(new XElement(STRINGS.IMAGE));
        }
        public void AddImage(EImage image)
        {
            if (image == null)
                throw new ArgumentNullException();

            if (Image != null)
                throw new Exception($"An Image has already been created for this Tile.");

            XElement.Add(image.XElement);
        }

        // FIXME: Is this supposed to be an ID attribute???
        public EObjectGroup ObjectGroup
        {
            get
            {
                var objectGroup = XElement.Element(STRINGS.OBJECTGROUP);
                return objectGroup != null ? new EObjectGroup(objectGroup) : null;
            }
        }
        public EObjectGroup CreateObjectGroup()
        {
            return new EObjectGroup(new XElement(STRINGS.OBJECTGROUP));
        }
        public void AddObjectGroup(EObjectGroup objectGroup)
        {
            if (objectGroup == null)
                throw new ArgumentNullException();

            if (ObjectGroup == null)
                throw new Exception($"An ObjectGroup has already been created for this Tile.");

            XElement.Add(objectGroup.XElement);
        }

        public EAnimation Animation
        {
            get
            {
                var animation = XElement.Element(STRINGS.ANIMATION);
                return animation != null ? new EAnimation(animation) : null;
            }
        }
        public EAnimation CreateAnimation()
        {
            return new EAnimation(new XElement(STRINGS.ANIMATION));
        }
        public void AddAnimation(EAnimation animation)
        {
            if (animation == null)
                throw new ArgumentNullException();

            if (Animation != null)
                throw new Exception($"An Animation has already been created for this Tile.");

            XElement.Add(animation.XElement);
        }
        #endregion

        public ETilesetTile(XElement element) : base(element)
        {
        }
    }
}
