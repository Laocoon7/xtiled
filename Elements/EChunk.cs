﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EChunk : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int X
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.X, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.X, value);
            }
        }
        public bool XExists => XElement.TryGetAttribute(STRINGS.X, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Y
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.Y, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.Y, value);
            }
        }
        public bool YExists => XElement.TryGetAttribute(STRINGS.Y, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Width
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WIDTH, value);
            }
        }
        public bool WidthExists => XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Height
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEIGHT, value);
            }
        }
        public bool HeightExists => XElement.TryGetAttribute(STRINGS.HEIGHT, -1, out int _);
        #endregion

        #region Elements
        public List<ELayerDataTile> Tiles
        {
            get
            {
                var returnValue = new List<ELayerDataTile>();
                foreach (var tile in XElement.Elements(STRINGS.TILE))
                {
                    returnValue.Add(new ELayerDataTile(tile));
                }

                return returnValue;
            }
        }
        public ELayerDataTile CreateTile()
        {
            return new ELayerDataTile(new XElement(STRINGS.TILE));
        }
        public void AddTile(ELayerDataTile tile)
        {
            if (tile == null)
                throw new ArgumentNullException();

            var tiles = Tiles;
            if (tiles.Count > 0)
                tiles[tiles.Count - 1].XElement.AddAfterSelf(tile.XElement);
            else
                XElement.Add(tile.XElement);
        }
        #endregion
        public EChunk(XElement element) : base(element)
        {
        }
    }
}
