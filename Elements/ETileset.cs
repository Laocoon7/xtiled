﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class ETileset : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int FirstGid
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.FIRSTGID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.FIRSTGID, value);
            }
        }
        public bool FirstGidExists => XElement.TryGetAttribute(STRINGS.FIRSTGID, -1, out int _);
        public string Source
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.SOURCE, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.SOURCE, value);
            }
        }
        public string Name
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NAME, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.NAME, value);
            }
        }
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int TileWidth
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILEWIDTH, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILEWIDTH, value);
            }
        }
        public bool TileWidthExists => XElement.TryGetAttribute(STRINGS.TILEWIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int TileHeight
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILEHEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILEHEIGHT, value);
            }
        }
        public bool TileHeightExists => XElement.TryGetAttribute(STRINGS.TILEHEIGHT, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Spacing
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.SPACING, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.SPACING, value);
            }
        }
        public bool SpacingExists => XElement.TryGetAttribute(STRINGS.SPACING, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Margin
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.MARGIN, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.MARGIN, value);
            }
        }
        public bool MarginExists => XElement.TryGetAttribute(STRINGS.MARGIN, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int TileCount
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILECOUNT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILECOUNT, value);
            }
        }
        public bool TileCountExists => XElement.TryGetAttribute(STRINGS.TILECOUNT, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Columns
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.COLUMNS, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.COLUMNS, value);
            }
        }
        public bool ColumnsExists => XElement.TryGetAttribute(STRINGS.COLUMNS, -1, out int _);
        public ObjectAlignment ObjectAlignment
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.OBJECTALIGNMENT, STRINGS.UNSPECIFIED, out string returnString))
                {
                    if (returnString == STRINGS.TOPLEFT)
                        return ObjectAlignment.TopLeft;
                    else if (returnString == STRINGS.TOP)
                        return ObjectAlignment.Top;
                    else if (returnString == STRINGS.TOPRIGHT)
                        return ObjectAlignment.TopRight;
                    else if (returnString == STRINGS.LEFT)
                        return ObjectAlignment.Left;
                    else if (returnString == STRINGS.CENTER)
                        return ObjectAlignment.Center;
                    else if (returnString == STRINGS.RIGHT)
                        return ObjectAlignment.Right;
                    else if (returnString == STRINGS.BOTTOMLEFT)
                        return ObjectAlignment.BottomLeft;
                    else if (returnString == STRINGS.BOTTOM)
                        return ObjectAlignment.Bottom;
                    else if (returnString == STRINGS.BOTTOMRIGHT)
                        return ObjectAlignment.BottomRight;
                    else
                        return ObjectAlignment.Unspecified;
                }

                return ObjectAlignment.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case ObjectAlignment.TopLeft:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.TOPLEFT);
                        break;
                    case ObjectAlignment.Top:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.TOP);
                        break;
                    case ObjectAlignment.TopRight:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.TOPRIGHT);
                        break;
                    case ObjectAlignment.Left:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.LEFT);
                        break;
                    case ObjectAlignment.Center:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.CENTER);
                        break;
                    case ObjectAlignment.Right:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.RIGHT);
                        break;
                    case ObjectAlignment.BottomLeft:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.BOTTOMLEFT);
                        break;
                    case ObjectAlignment.Bottom:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.BOTTOM);
                        break;
                    case ObjectAlignment.BottomRight:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.BOTTOMRIGHT);
                        break;
                    default:
                    case ObjectAlignment.Unspecified:
                        XElement.SetAttributeValue(STRINGS.OBJECTALIGNMENT, STRINGS.UNSPECIFIED);
                        break;
                }
            }
        }
        #endregion

        #region Elements
        public EImage Image
        {
            get
            {
                var image = XElement.Element(STRINGS.IMAGE);
                return image != null ? new EImage(image) : null;
            }
        }
        public EImage CreateData()
        {
            return new EImage(new XElement(STRINGS.IMAGE));
        }
        public void AddImage(EImage image)
        {
            if (image == null)
                throw new ArgumentNullException();

            if (Image != null)
                throw new Exception($"An Image has already been created for this Tileset.");

            XElement.Add(image.XElement);
        }

        public ETileOffset TileOffset
        {
            get
            {
                var tileOffset = XElement.Element(STRINGS.TILEOFFSET);
                return tileOffset != null ? new ETileOffset(tileOffset) : null;
            }
        }
        public ETileOffset CreateTileOffset()
        {
            return new ETileOffset(new XElement(STRINGS.TILEOFFSET));
        }
        public void AddTileOffset(ETileOffset tileOffset)
        {
            if (tileOffset == null)
                throw new ArgumentNullException();

            if (TileOffset != null)
                throw new Exception($"A TileOffset has already been created for this Tileset.");

            XElement.Add(tileOffset.XElement);
        }

        public EGrid Grid
        {
            get
            {
                var grid = XElement.Element(STRINGS.GRID);
                return grid != null ? new EGrid(grid) : null;
            }
        }
        public EGrid CreateGrid()
        {
            return new EGrid(new XElement(STRINGS.GRID));
        }
        public void AddGrid(EGrid grid)
        {
            if (grid == null)
                throw new ArgumentNullException();

            if (Grid != null)
                throw new Exception($"A Grid has already been created for this Tileset.");

            XElement.Add(grid.XElement);
        }

        public EProperties Properties
        {
            get
            {
                var properties = XElement.Element(STRINGS.PROPERTIES);
                return properties != null ? new EProperties(properties) : null;
            }
        }
        public EProperties CreateProperties()
        {
            return new EProperties(new XElement(STRINGS.PROPERTIES));
        }
        public void AddProperties(EProperties properties)
        {
            if (properties == null)
                throw new ArgumentNullException();

            if (Properties != null)
                throw new Exception($"Properties already exists on this Tileset.");

            XElement.Add(properties.XElement);
        }

        public ETerrainTypes TerrainTypes
        {
            get
            {
                var terrainTypes = XElement.Element(STRINGS.TERRAINTYPES);
                return terrainTypes != null ? new ETerrainTypes(terrainTypes) : null;
            }
        }
        public ETerrainTypes CreateTerrainTypes()
        {
            return new ETerrainTypes(new XElement(STRINGS.TERRAINTYPES));
        }
        public void AddTerrainTypes(ETerrainTypes terrainTypes)
        {
            if (terrainTypes == null)
                throw new ArgumentNullException();

            if (TerrainTypes != null)
                throw new Exception($"TerrainTypes already exists on this Tileset.");

            XElement.Add(terrainTypes.XElement);
        }

        public EWangSets WangSets
        {
            get
            {
                var wangSets = XElement.Element(STRINGS.WANGSETS);
                return wangSets != null ? new EWangSets(wangSets) : null;
            }
        }
        public EWangSets CreateWangSets()
        {
            return new EWangSets(new XElement(STRINGS.WANGSETS));
        }
        public void AddWangSets(EWangSets wangSets)
        {
            if (wangSets == null)
                throw new ArgumentNullException();

            if (WangSets != null)
                throw new Exception($"WangSets already exists on this Tileset.");

            XElement.Add(wangSets.XElement);
        }

        public List<ETilesetTile> Tiles
        {
            get
            {
                var returnValue = new List<ETilesetTile>();
                foreach (var tile in XElement.Elements(STRINGS.TILE))
                {
                    returnValue.Add(new ETilesetTile(tile));
                }

                return returnValue;
            }
        }
        public ETilesetTile CreateTile()
        {
            return new ETilesetTile(new XElement(STRINGS.TILE));
        }
        public void AddTile(ETilesetTile tile)
        {
            if (tile == null)
                throw new ArgumentNullException();

            var tiles = Tiles;
            if (tiles.Count > 0)
                tiles[tiles.Count - 1].XElement.AddAfterSelf(tile.XElement);
            else
                XElement.Add(tile.XElement);
        }
        #endregion

        public ETileset(XElement element) : base(element)
        {
        }
    }
}
