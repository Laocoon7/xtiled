﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class EWangSets : ElementBase
    {
        #region Attributes
        #endregion

        #region Elements
        public List<EWangSet> WangSets
        {
            get
            {
                var returnValue = new List<EWangSet>();
                foreach (var wangSet in XElement.Elements(STRINGS.WANGSET))
                {
                    returnValue.Add(new EWangSet(wangSet));
                }

                return returnValue;
            }
        }
        public EWangSet CreateWangSet()
        {
            return new EWangSet(new XElement(STRINGS.WANGSET));
        }
        public void AddWangSet(EWangSet wangSet)
        {
            if (wangSet == null)
                throw new ArgumentNullException();

            var wangSets = WangSets;
            if (wangSets.Count > 0)
                wangSets[wangSets.Count - 1].XElement.AddAfterSelf(wangSet.XElement);
            else
                XElement.Add(wangSet.XElement);
        }
        #endregion

        public EWangSets(XElement element) : base(element)
        {
        }
    }
}
