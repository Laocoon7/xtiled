﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class EAnimation : ElementBase
    {
        #region Attributes
        #endregion

        #region Elements
        public List<EFrame> Frames
        {
            get
            {
                var frames = new List<EFrame>();
                foreach (var frame in XElement.Elements(STRINGS.FRAME))
                {
                    frames.Add(new EFrame(frame));
                }

                return frames;
            }
        }
        public EFrame CreateFrame()
        {
            return new EFrame(new XElement(STRINGS.FRAME));
        }
        public void AddFrame(EFrame frame)
        {
            if (frame == null)
                throw new ArgumentNullException();

            var frames = Frames;
            if (frames.Count > 0)
                frames[frames.Count - 1].XElement.AddAfterSelf(frame.XElement);
            else
                XElement.Add(frame.XElement);
        }
        public void AddFrameBefore(EFrame frame, EFrame existingFrame)
        {
            if (frame == null || existingFrame == null)
                throw new ArgumentNullException();

            existingFrame.XElement.AddBeforeSelf(frame.XElement);
        }
        public void AddFrameAfter(EFrame frame, EFrame existingFrame)
        {
            if (frame == null || existingFrame == null)
                throw new ArgumentNullException();

            existingFrame.XElement.AddAfterSelf(frame.XElement);
        }
        #endregion

        public EAnimation(XElement element) : base(element)
        {
        }
    }
}
