﻿using System;
using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class ETerrain : ElementBase
    {
        #region Attributes
        public string Name
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NAME, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.NAME, value);
            }
        }
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Tile
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILE, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILE, value);
            }
        }
        public bool TileExists => XElement.TryGetAttribute(STRINGS.TILE, -1, out int _);
        #endregion

        #region Elements
        public EProperties Properties
        {
            get
            {
                var properties = XElement.Element(STRINGS.PROPERTIES);
                return properties != null ? new EProperties(properties) : null;
            }
        }
        public EProperties CreateProperties()
        {
            return new EProperties(new XElement(STRINGS.PROPERTIES));
        }
        public void AddProperties(EProperties properties)
        {
            if (properties == null)
                throw new ArgumentNullException();

            if (Properties != null)
                throw new Exception($"Properties already exists on this Terrain.");

            XElement.Add(properties.XElement);
        }
        #endregion

        public ETerrain(XElement element) : base(element)
        {
        }
    }
}
