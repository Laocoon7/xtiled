﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EMap : ElementBase
    {
        #region Attributes
        public string Version
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.VERSION, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.VERSION, value);
            }
        }
        public string TiledVersion
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILEDVERSION, null, out string returnString);
                return returnString;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILEDVERSION, value);
            }
        }
        public Orientation Orientation
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.ORIENTATION, STRINGS.ORTHOGONAL, out string returnString))
                {
                    if (returnString == STRINGS.ISOMETRIC)
                        return Orientation.Isometric;
                    else if (returnString == STRINGS.STAGGERED)
                        return Orientation.Staggered;
                    else if (returnString == STRINGS.HEXAGONAL)
                        return Orientation.Hexagonal;
                    else
                        return Orientation.Orthogonal;
                }

                return Orientation.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case Orientation.Isometric:
                        XElement.SetAttributeValue(STRINGS.ORIENTATION, STRINGS.ISOMETRIC);
                        break;
                    case Orientation.Staggered:
                        XElement.SetAttributeValue(STRINGS.ORIENTATION, STRINGS.STAGGERED);
                        break;
                    case Orientation.Hexagonal:
                        XElement.SetAttributeValue(STRINGS.ORIENTATION, STRINGS.HEXAGONAL);
                        break;
                    default:
                    case Orientation.Orthogonal:
                        XElement.SetAttributeValue(STRINGS.ORIENTATION, STRINGS.ORTHOGONAL);
                        break;
                }
            }
        }
        public RenderOrder RenderOrder
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.RENDERORDER, STRINGS.RIGHTDOWN, out string returnString))
                {
                    if (returnString == STRINGS.RIGHTUP)
                        return RenderOrder.RightUp;
                    else if (returnString == STRINGS.LEFTDOWN)
                        return RenderOrder.LeftDown;
                    else if (returnString == STRINGS.LEFTUP)
                        return RenderOrder.LeftUp;
                    else
                        return RenderOrder.RightDown;
                }

                return RenderOrder.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case RenderOrder.RightUp:
                        XElement.SetAttributeValue(STRINGS.RENDERORDER, STRINGS.RIGHTUP);
                        break;
                    case RenderOrder.LeftDown:
                        XElement.SetAttributeValue(STRINGS.RENDERORDER, STRINGS.LEFTDOWN);
                        break;
                    case RenderOrder.LeftUp:
                        XElement.SetAttributeValue(STRINGS.RENDERORDER, STRINGS.LEFTUP);
                        break;
                    default:
                    case RenderOrder.RightDown:
                        XElement.SetAttributeValue(STRINGS.RENDERORDER, STRINGS.RIGHTDOWN);
                        break;
                }
            }
        }
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int CompressionLevel
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.COMPRESSIONLEVEL, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.COMPRESSIONLEVEL, value);
            }
        }
        public bool CompressionLevelExists => XElement.TryGetAttribute(STRINGS.COMPRESSIONLEVEL, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Width
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WIDTH, value);
            }
        }
        public bool WidthExists => XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Height
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEIGHT, value);
            }
        }
        public bool HeightExists => XElement.TryGetAttribute(STRINGS.HEIGHT, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int TileWidth
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILEWIDTH, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILEWIDTH, value);
            }
        }
        public bool TileWidthExists => XElement.TryGetAttribute(STRINGS.TILEWIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int TileHeight
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILEHEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILEHEIGHT, value);
            }
        }
        public bool TileHeightExists => XElement.TryGetAttribute(STRINGS.TILEHEIGHT, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int HexsideLength
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEXSIDELENGTH, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEXSIDELENGTH, value);
            }
        }
        public bool HexsideLengthExists => XElement.TryGetAttribute(STRINGS.HEXSIDELENGTH, -1, out int _);
        public StaggerAxis StaggerAxis
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.STAGGERAXIS, STRINGS.X, out string returnString))
                {
                    if (returnString == STRINGS.Y)
                        return StaggerAxis.Y;
                    else
                        return StaggerAxis.X;
                }

                return StaggerAxis.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case StaggerAxis.Y:
                        XElement.SetAttributeValue(STRINGS.STAGGERAXIS, STRINGS.Y);
                        break;
                    default:
                    case StaggerAxis.X:
                        XElement.SetAttributeValue(STRINGS.STAGGERAXIS, STRINGS.X);
                        break;
                }
            }
        }
        public StaggerIndex StaggerIndex
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.STAGGERINDEX, STRINGS.EVEN, out string returnString))
                {
                    if (returnString == STRINGS.ODD)
                        return StaggerIndex.Odd;
                    else
                        return StaggerIndex.Even;
                }

                return StaggerIndex.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case StaggerIndex.Odd:
                        XElement.SetAttributeValue(STRINGS.STAGGERINDEX, STRINGS.ODD);
                        break;
                    default:
                    case StaggerIndex.Even:
                        XElement.SetAttributeValue(STRINGS.STAGGERINDEX, STRINGS.EVEN);
                        break;
                }
            }
        }
        public Color BackgroundColor
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.BACKGROUNDCOLOR, null, out string colorString))
                    return new Color(colorString);

                return null;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.BACKGROUNDCOLOR, value.ToString());
            }
        }
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int NextLayerId
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NEXTLAYERID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.NEXTLAYERID, value);
            }
        }
        public bool NextLayerIdExists => XElement.TryGetAttribute(STRINGS.NEXTLAYERID, -1, out int _);
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int NextObjectId
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NEXTOBJECTID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.NEXTOBJECTID, value);
            }
        }
        public bool NextObjectIdExists => XElement.TryGetAttribute(STRINGS.NEXTOBJECTID, -1, out int _);
        public bool Infinite
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.INFINITE, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.INFINITE, value);
            }
        }
        public bool InfiniteExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.INFINITE, false, out _);
            }
        }
        #endregion

        #region Elements
        public List<EEditorSettings> EditorSettings
        {
            get
            {
                var returnValue = new List<EEditorSettings>();
                foreach (var editorSettings in XElement.Elements(STRINGS.EDITORSETTINGS))
                {
                    returnValue.Add(new EEditorSettings(editorSettings));
                }

                return returnValue;
            }
        }
        public EEditorSettings CreateEditorSettings()
        {
            return new EEditorSettings(new XElement(STRINGS.EDITORSETTINGS));
        }
        public void AddEditorSettings(EEditorSettings editorSettings)
        {
            if (editorSettings == null)
                throw new ArgumentNullException();

            var oldEditorSettings = EditorSettings;
            if (oldEditorSettings.Count > 0)
                oldEditorSettings[oldEditorSettings.Count - 1].XElement.AddAfterSelf(editorSettings.XElement);
            else
                XElement.Add(editorSettings.XElement);
        }

        public List<ETileset> Tilesets
        {
            get
            {
                var returnValue = new List<ETileset>();
                foreach (var tileset in XElement.Elements(STRINGS.TILESET))
                {
                    returnValue.Add(new ETileset(tileset));
                }

                return returnValue;
            }
        }
        public ETileset CreateTileset()
        {
            return new ETileset(new XElement(STRINGS.TILESET));
        }
        public void AddTileset(ETileset tileset)
        {
            if (tileset == null)
                throw new ArgumentNullException();

            var tilesets = Tilesets;
            if (tilesets.Count > 0)
            {
                tilesets[tilesets.Count - 1].XElement.AddAfterSelf(tileset.XElement);
                return;
            }

            // All Tilesets should appear before any layers
            var allLayers = AllLayers;
            if (allLayers.Count > 0)
                allLayers[0].XElement.AddBeforeSelf(tileset.XElement);
            else
                XElement.Add(tileset.XElement);
        }

        public List<ELayerBase> AllLayers
        {
            get
            {
                var returnValue = new List<ELayerBase>();
                foreach (var layer in XElement.Elements().Where(x => x.Name.LocalName == STRINGS.LAYER || x.Name.LocalName == STRINGS.OBJECTGROUP || x.Name.LocalName == STRINGS.IMAGELAYER || x.Name.LocalName == STRINGS.GROUP))
                {
                    switch (layer.Name.LocalName)
                    {
                        case STRINGS.LAYER:
                            returnValue.Add(new ELayer(layer));
                            break;
                        case STRINGS.OBJECTGROUP:
                            returnValue.Add(new EObjectGroup(layer));
                            break;
                        case STRINGS.IMAGELAYER:
                            returnValue.Add(new EObjectGroup(layer));
                            break;
                        case STRINGS.GROUP:
                            returnValue.Add(new EGroup(layer));
                            break;
                    }
                }
                
                return returnValue;
            }
        }
        public void AddLayer(ELayerBase layer)
        {
            if (layer == null)
                throw new ArgumentNullException();

            var allLayers = AllLayers;
            if (allLayers.Count > 0)
            {
                AddLayerAfter(layer, allLayers[allLayers.Count - 1]);
                return;
            }

            var tilesets = Tilesets;
            if (tilesets.Count > 0)
                tilesets[tilesets.Count - 1].XElement.AddAfterSelf(layer.XElement);
            else
                XElement.Add(layer);
        }
        public void AddLayerBefore(ELayerBase layer, ELayerBase existingLayer)
        {
            if (layer == null || existingLayer == null)
                throw new ArgumentNullException();

            existingLayer.XElement.AddBeforeSelf(layer.XElement);
        }
        public void AddLayerAfter(ELayerBase layer, ELayerBase existingLayer)
        {
            if (layer == null || existingLayer == null)
                throw new ArgumentNullException();

            existingLayer.XElement.AddAfterSelf(layer.XElement);
        }

        public List<ELayer> Layers
        {
            get
            {
                var returnValue = new List<ELayer>();
                foreach (var layer in XElement.Elements(STRINGS.LAYER))
                {
                    returnValue.Add(new ELayer(layer));
                }

                return returnValue;
            }
        }
        public ELayer CreateLayer()
        {
            return new ELayer(new XElement(STRINGS.LAYER));
        }

        public List<EObjectGroup> ObjectGroups
        {
            get
            {
                var returnValue = new List<EObjectGroup>();
                foreach (var layer in XElement.Elements(STRINGS.OBJECTGROUP))
                {
                    returnValue.Add(new EObjectGroup(layer));
                }

                return returnValue;
            }
        }
        public EObjectGroup CreateObjectGroup()
        {
            return new EObjectGroup(new XElement(STRINGS.OBJECTGROUP));
        }

        public List<EImageLayer> ImageLayers
        {
            get
            {
                var returnValue = new List<EImageLayer>();
                foreach (var layer in XElement.Elements(STRINGS.IMAGELAYER))
                {
                    returnValue.Add(new EImageLayer(layer));
                }

                return returnValue;
            }
        }
        public EImageLayer CreateImageLayer()
        {
            return new EImageLayer(new XElement(STRINGS.IMAGELAYER));
        }

        public List<EGroup> Groups
        {
            get
            {
                var returnValue = new List<EGroup>();
                foreach (var layer in XElement.Elements(STRINGS.GROUP))
                {
                    returnValue.Add(new EGroup(layer));
                }

                return returnValue;
            }
        }
        public EGroup CreateGroup()
        {
            return new EGroup(new XElement(STRINGS.GROUP));
        }

        public EProperties Properties
        {
            get
            {
                var properties = XElement.Element(STRINGS.PROPERTIES);
                return properties != null ? new EProperties(properties) : null;
            }
        }
        public EProperties CreateProperties()
        {
            return new EProperties(new XElement(STRINGS.PROPERTIES));
        }
        public void AddProperties(EProperties properties)
        {
            if (properties == null)
                throw new ArgumentNullException();

            if (Properties != null)
                throw new Exception($"Properties already exists on this Map.");

            XElement.Add(properties.XElement);
        }
        #endregion

        public EMap(XElement element) : base(element)
        {
        }
    }
}
