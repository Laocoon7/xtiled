﻿using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class ETileOffset : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int X
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.X, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.X, value);
            }
        }
        public bool XExists => XElement.TryGetAttribute(STRINGS.X, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Y
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.Y, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.Y, value);
            }
        }
        public bool YExists => XElement.TryGetAttribute(STRINGS.Y, -1, out int _);
        #endregion

        #region Elements
        #endregion
        public ETileOffset(XElement element) : base(element)
        {
        }
    }
}
