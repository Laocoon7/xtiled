﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class EProperties : ElementBase
    {
        #region Attributes
        #endregion

        #region Elements
        public List<EProperty> Properties
        {
            get
            {
                var returnValue = new List<EProperty>();
                foreach (var property in XElement.Elements(STRINGS.PROPERTY))
                {
                    returnValue.Add(new EProperty(property));
                }

                return returnValue;
            }
        }
        public EProperty CreateProperty()
        {
            return new EProperty(new XElement(STRINGS.PROPERTY));
        }
        public void AddProperty(EProperty property)
        {
            if (property == null)
                throw new ArgumentNullException();

            var oldProperties = Properties;
            if (oldProperties.Count > 0)
                oldProperties[oldProperties.Count - 1].XElement.AddAfterSelf(property.XElement);
            else
                XElement.Add(property.XElement);
        }
        #endregion


        public EProperties(XElement element) : base(element)
        {
        }
    }
}
