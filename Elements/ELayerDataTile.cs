﻿using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class ELayerDataTile : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Gid
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.GID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.GID, value);
            }
        }
        public bool GidExists => XElement.TryGetAttribute(STRINGS.GID, -1, out int _);
        #endregion

        #region Elements
        #endregion
        public ELayerDataTile(XElement element) : base(element)
        {
        }
    }
}
