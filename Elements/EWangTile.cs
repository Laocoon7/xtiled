﻿using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EWangTile : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int TileId
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILEID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILEID, value);
            }
        }
        public bool TileIdExists => XElement.TryGetAttribute(STRINGS.TILEID, -1, out int _);

        // FIXME: UInt32
        public string WangId
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WANGID, null, out string returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WANGID, value);
            }
        }
        public bool WangIdExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.WANGID, false, out _);
            }
        }

        public bool HFlip
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HFLIP, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HFLIP, value);
            }
        }
        public bool HFlipExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.HFLIP, false, out _);
            }
        }
        public bool VFlip
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.VFLIP, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.VFLIP, value);
            }
        }
        public bool VFlipExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.VFLIP, false, out _);
            }
        }
        public bool DFlip
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.DFLIP, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.DFLIP, value);
            }
        }
        public bool DFlipExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.DFLIP, false, out _);
            }
        }
        #endregion

        #region Elements
        #endregion
        public EWangTile(XElement element) : base(element)
        {
        }
    }
}
