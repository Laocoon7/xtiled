﻿using System;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class EPoint : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to (EObject)Parent.X
        /// </summary>
        public int X
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).X;
                }
                catch
                {
                    throw new Exception($"This Point does not have a parent object.");
                }
            }

            set
            {
                try
                {
                    new EObject(XElement.Parent).X = value;
                }
                catch
                {
                    throw new Exception($"This Point does not have a parent object.");
                }
            }
        }
        public bool XExists
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).XExists;
                }
                catch
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Defaults to (EObject)Parent.Y
        /// </summary>
        public int Y
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).Y;
                }
                catch
                {
                    throw new Exception($"This Point does not have a parent object.");
                }
            }

            set
            {
                try
                {
                    new EObject(XElement.Parent).Y = value;
                }
                catch
                {
                    throw new Exception($"This Point does not have a parent object.");
                }
            }
        }
        public bool YExists
        {
            get
            {
                try
                {
                    return new EObject(XElement.Parent).YExists;
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion

        #region Elements
        #endregion

        public EPoint(XElement element) : base(element)
        {
        }
    }
}
