﻿using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EGrid : ElementBase
    {
        #region Attributes
        public Orientation Orientation
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.ORIENTATION, STRINGS.ORTHOGONAL, out string returnString))
                {
                    if (returnString == STRINGS.ISOMETRIC)
                        return Orientation.Isometric;
                    else
                        return Orientation.Orthogonal;
                }

                return Orientation.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case Orientation.Isometric:
                        XElement.SetAttributeValue(STRINGS.ORIENTATION, STRINGS.ISOMETRIC);
                        break;
                    default:
                    case Orientation.Orthogonal:
                        XElement.SetAttributeValue(STRINGS.ORIENTATION, STRINGS.ORTHOGONAL);
                        break;
                }
            }
        }
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Width
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WIDTH, value);
            }
        }
        public bool WidthExists => XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Height
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEIGHT, value);
            }
        }
        public bool HeightExists => XElement.TryGetAttribute(STRINGS.HEIGHT, -1, out int _);
        #endregion

        #region Elements
        #endregion
        public EGrid(XElement element) : base(element)
        {
        }
    }
}
