﻿using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EText : ElementBase
    {
        #region Attributes
        public string FontFamily
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.FONTFAMILY, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.FONTFAMILY, value);
            }
        }
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int PixelSize
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.PIXELSIZE, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.PIXELSIZE, value);
            }
        }
        public bool PixelSizeExists => XElement.TryGetAttribute(STRINGS.PIXELSIZE, -1, out int _);
        public bool Wrap
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WRAP, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WRAP, value);
            }
        }
        public bool WrapExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.WRAP, false, out _);
            }
        }
        public Color Color
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.COLOR, null, out string colorString))
                    return new Color(colorString);

                return null;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.COLOR, value.ToString());
            }
        }
        public bool Bold
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.BOLD, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.BOLD, value);
            }
        }
        public bool BoldExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.BOLD, false, out _);
            }
        }
        public bool Italic
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.ITALIC, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.ITALIC, value);
            }
        }
        public bool ItalicExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.ITALIC, false, out _);
            }
        }
        public bool Underline
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.UNDERLINE, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.UNDERLINE, value);
            }
        }
        public bool UnderlineExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.UNDERLINE, false, out _);
            }
        }
        public bool Strikeout
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.STRIKEOUT, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.STRIKEOUT, value);
            }
        }
        public bool StrikeoutExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.STRIKEOUT, false, out _);
            }
        }
        public bool Kerning
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.KERNING, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.KERNING, value);
            }
        }
        public bool KerningExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.KERNING, false, out _);
            }
        }
        public HAlign HAlign
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.HALIGN, STRINGS.LEFT, out string returnString))
                {
                    if (returnString == STRINGS.CENTER)
                        return HAlign.Center;
                    else if (returnString == STRINGS.RIGHT)
                        return HAlign.Right;
                    else if (returnString == STRINGS.JUSTIFY)
                        return HAlign.Justify;
                    else
                        return HAlign.Left;
                }

                return HAlign.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case HAlign.Center:
                        XElement.SetAttributeValue(STRINGS.HALIGN, STRINGS.CENTER);
                        break;
                    case HAlign.Right:
                        XElement.SetAttributeValue(STRINGS.HALIGN, STRINGS.RIGHT);
                        break;
                    case HAlign.Justify:
                        XElement.SetAttributeValue(STRINGS.HALIGN, STRINGS.JUSTIFY);
                        break;
                    default:
                    case HAlign.Left:
                        XElement.SetAttributeValue(STRINGS.HALIGN, STRINGS.LEFT);
                        break;
                }
            }
        }
        public VAlign VAlign
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.VALIGN, STRINGS.TOP, out string returnString))
                {
                    if (returnString == STRINGS.CENTER)
                        return VAlign.Center;
                    else if (returnString == STRINGS.BOTTOM)
                        return VAlign.Bottom;
                    else
                        return VAlign.Top;
                }

                return VAlign.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case VAlign.Center:
                        XElement.SetAttributeValue(STRINGS.VALIGN, STRINGS.CENTER);
                        break;
                    case VAlign.Bottom:
                        XElement.SetAttributeValue(STRINGS.VALIGN, STRINGS.BOTTOM);
                        break;
                    default:
                    case VAlign.Top:
                        XElement.SetAttributeValue(STRINGS.VALIGN, STRINGS.TOP);
                        break;
                }
            }
        }
        #endregion

        #region Elements
        #endregion
        public EText(XElement element) : base(element)
        {
        }
    }
}
