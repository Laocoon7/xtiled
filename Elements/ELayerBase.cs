﻿using System;
using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class ELayerBase : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Id
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.ID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.ID, value);
            }
        }
        public bool IdExists => XElement.TryGetAttribute(STRINGS.ID, -1, out int _);
        public string Name
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NAME, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.NAME, value);
            }
        }
        /// <summary>
        /// Defaults to 1
        /// </summary>
        public float Opacity
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.OPACITY, 1, out float returnFloat);
                return returnFloat;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.OPACITY, value);
            }
        }
        public bool OpacityExists => XElement.TryGetAttribute(STRINGS.OPACITY, -1, out int _);
        public bool Visible
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.VISIBLE, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.VISIBLE, value);
            }
        }
        public bool VisibleExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.VISIBLE, false, out _);
            }
        }
        public Color TintColor
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.TINTCOLOR, null, out string colorString))
                    return new Color(colorString);

                return null;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TINTCOLOR, value.ToString());
            }
        }
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int OffsetX
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.OFFSETX, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.OFFSETX, value);
            }
        }
        public bool OffsetXExists => XElement.TryGetAttribute(STRINGS.OFFSETX, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int OffsetY
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.OFFSETY, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.OFFSETY, value);
            }
        }
        public bool OffsetYExists => XElement.TryGetAttribute(STRINGS.OFFSETY, -1, out int _);
        #endregion

        #region Elements
        public EProperties Properties
        {
            get
            {
                var properties = XElement.Element(STRINGS.PROPERTIES);
                return properties != null ? new EProperties(properties) : null;
            }
        }
        public EProperties CreateProperties()
        {
            return new EProperties(new XElement(STRINGS.PROPERTIES));
        }
        public void AddProperties(EProperties properties)
        {
            if (properties == null)
                throw new ArgumentNullException();

            if (Properties != null)
                throw new Exception($"Properties already exists on this Layer.");

            XElement.Add(properties.XElement);
        }
        #endregion

        public ELayerBase(XElement element) : base(element)
        {
        }
    }
}
