﻿using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EProperty : ElementBase
    {
        #region Attributes
        public string Name
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NAME, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.NAME, value);
            }
        }
        public string Type
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TYPE, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.TYPE, value);
            }
        }
        /// <summary>
        /// Returns a value based on <see cref="EProperty.Type"/>. 
        /// (<see cref="int"/>, <see cref="float"/>, <see cref="bool"/>, <see cref="Color"/>, <see cref="string"/>)
        /// (defaults to <see cref="string"/>)
        /// (NOTE: Multi-lined strings should use <see cref="EProperty.InnerValue"/>.)
        /// </summary>
        public dynamic Value
        {
            get
            {
                return GetValue(false);
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.VALUE, value);
            }
        }
        public bool ValueExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.VALUE, null, out string _);
            }
        }

        /// <summary>
        /// Returns a value based on <see cref="EProperty.Type"/>. 
        /// (<see cref="int"/>, <see cref="float"/>, <see cref="bool"/>, <see cref="Color"/>, <see cref="string"/>)
        /// (defaults to <see cref="string"/>)
        /// (NOTE: Multi-lined strings should use <see cref="EProperty.InnerValue"/>.)
        /// </summary>
        /// <param name="forceString">Set to true to guarantee return value of type <see cref="string"/>.</param>
        /// <returns></returns>
        public dynamic GetValue(bool forceString = false)
        {
            string type = Type;
            if (forceString)
                type = STRINGS.STRING;

            switch (type)
            {
                case STRINGS.INT:
                    XElement.TryGetAttribute(STRINGS.VALUE, -1, out int returnInt);
                    return returnInt;
                case STRINGS.FLOAT:
                    XElement.TryGetAttribute(STRINGS.VALUE, -1, out float returnFloat);
                    return returnFloat;
                case STRINGS.BOOL:
                    XElement.TryGetAttribute(STRINGS.VALUE, false, out bool returnBool);
                    return returnBool;
                case STRINGS.COLOR:
                    XElement.TryGetAttribute(STRINGS.VALUE, null, out string returnColor);
                    return new Color(returnColor);
                case STRINGS.FILE:
                    XElement.TryGetAttribute(STRINGS.VALUE, null, out string returnFile);
                    return returnFile;
                case STRINGS.OBJECT:
                    XElement.TryGetAttribute(STRINGS.VALUE, -1, out int returnObject);
                    return returnObject;
                default:
                case STRINGS.STRING:
                    // If the value attribute doesn't exist
                    XElement.TryGetAttribute(STRINGS.VALUE, null, out string returnString);
                    return returnString;
            }
        }
        #endregion

        #region Elements
        #endregion

        public EProperty(XElement element) : base(element)
        {
            string x = Value();
        }
    }
}
