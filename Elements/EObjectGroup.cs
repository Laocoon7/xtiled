﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EObjectGroup : ELayerBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int X
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.X, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.X, value);
            }
        }
        public bool XExists => XElement.TryGetAttribute(STRINGS.X, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Y
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.Y, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.Y, value);
            }
        }
        public bool YExists => XElement.TryGetAttribute(STRINGS.Y, -1, out int _);
        public Color Color
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.COLOR, null, out string colorString))
                    return new Color(colorString);

                return null;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.COLOR, value.ToString());
            }
        }
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Width
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WIDTH, value);
            }
        }
        public bool WidthExists => XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Height
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEIGHT, value);
            }
        }
        public bool HeightExists => XElement.TryGetAttribute(STRINGS.HEIGHT, -1, out int _);
        public DrawOrder DrawOrder
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.DRAWORDER, STRINGS.TOPDOWN, out string returnString))
                {
                    if (returnString == STRINGS.INDEX)
                        return DrawOrder.Index;
                    else
                        return DrawOrder.TopDown;
                }

                return DrawOrder.DoesNotExist;
            }

            set
            {
                switch (value)
                {
                    case DrawOrder.Index:
                        XElement.SetAttributeValue(STRINGS.STAGGERINDEX, STRINGS.ODD);
                        break;
                    default:
                    case DrawOrder.TopDown:
                        XElement.SetAttributeValue(STRINGS.STAGGERINDEX, STRINGS.EVEN);
                        break;
                }
            }
        }
        #endregion

        #region Elements
        public List<EObject> Objects
        {
            get
            {
                var returnValue = new List<EObject>();
                foreach (var obj in XElement.Elements(STRINGS.OBJECT))
                {
                    returnValue.Add(new EObject(obj));
                }

                return returnValue;
            }
        }
        public EObject CreateObject()
        {
            return new EObject(new XElement(STRINGS.OBJECT));
        }
        public void AddObject(EObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException();

            var objects = Objects;
            if (objects.Count > 0)
                objects[objects.Count - 1].XElement.AddAfterSelf(obj.XElement);
            else
                XElement.Add(obj.XElement);
        }
        #endregion

        public EObjectGroup(XElement element) : base(element)
        {
        }
    }
}
