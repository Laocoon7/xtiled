﻿using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EChunkSize : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Width
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WIDTH, value);
            }
        }
        public bool WidthExists => XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Height
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEIGHT, value);
            }
        }
        public bool HeightExists => XElement.TryGetAttribute(STRINGS.HEIGHT, -1, out int _);
        #endregion

        #region Elements
        #endregion

        public EChunkSize(XElement element) : base(element)
        {
        }
    }
}
