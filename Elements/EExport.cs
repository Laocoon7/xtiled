﻿using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EExport : ElementBase
    {
        #region Attributes
        public string Target
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TARGET, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.TARGET, value);
            }
        }
        public string Format
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.FORMAT, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.FORMAT, value);
            }
        }
        #endregion

        #region Elements
        #endregion

        public EExport(XElement element) : base(element)
        {
        }
    }
}
