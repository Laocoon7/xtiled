﻿using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EFrame : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int TileId
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILEID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILEID, value);
            }
        }
        public bool TileIdExists => XElement.TryGetAttribute(STRINGS.TILEID, -1, out int _);
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Duration
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.DURATION, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.DURATION, value);
            }
        }
        public bool DurationExists => XElement.TryGetAttribute(STRINGS.DURATION, -1, out int _);
        #endregion

        #region Elements
        #endregion

        public EFrame(XElement element) : base(element)
        {
        }
    }
}
