﻿using System;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class ETemplate : ElementBase
    {
        #region Attributes
        #endregion

        #region Elements
        public ETileset Tileset
        {
            get
            {
                var tileset = XElement.Element(STRINGS.TILESET);
                return tileset != null ? new ETileset(tileset) : null;
            }
        }
        public ETileset CreateTileset()
        {
            return new ETileset(new XElement(STRINGS.TILESET));
        }
        public void AddTileset(ETileset tileset)
        {
            if (tileset == null)
                throw new ArgumentNullException();

            if (Tileset != null)
                throw new Exception($"A Tileset already exists on this Template.");

            XElement.Add(tileset.XElement);
        }

        public EObject Object
        {
            get
            {
                var obj = XElement.Element(STRINGS.OBJECT);
                return obj != null ? new EObject(obj) : null;
            }
        }
        public EObject CreateObject()
        {
            return new EObject(new XElement(STRINGS.OBJECT));
        }
        public void AddObject(EObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException();

            if (Object != null)
                throw new Exception($"An Object already exists on this Template.");

            XElement.Add(obj.XElement);
        }
        #endregion

        public ETemplate(XElement element) : base(element)
        {
        }
    }
}
