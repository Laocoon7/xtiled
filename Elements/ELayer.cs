﻿using System;
using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class ELayer : ELayerBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int X
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.X, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.X, value);
            }
        }
        public bool XExists => XElement.TryGetAttribute(STRINGS.X, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Y
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.Y, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.Y, value);
            }
        }
        public bool YExists => XElement.TryGetAttribute(STRINGS.Y, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Width
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WIDTH, value);
            }
        }
        public bool WidthExists => XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Height
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEIGHT, value);
            }
        }
        public bool HeightExists => XElement.TryGetAttribute(STRINGS.HEIGHT, -1, out int _);
        #endregion

        #region Elements
        public EData Data
        {
            get
            {
                var data = XElement.Element(STRINGS.DATA);
                return data != null ? new EData(data) : null;
            }
        }
        public EData CreateData()
        {
            return new EData(new XElement(STRINGS.DATA));
        }
        public void AddData(EData data)
        {
            if (data == null)
                throw new ArgumentNullException();

            if (Data != null)
                throw new Exception($"Data has already been created for this Layer.");

            XElement.Add(data.XElement);
        }
        #endregion

        public ELayer(XElement element) : base(element)
        {
        }
    }
}
