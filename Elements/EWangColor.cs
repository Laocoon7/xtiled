﻿using System.Xml.Linq;
using Citadel.XTiled.DataTypes;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EWangColor : ElementBase
    {
        #region Attributes
        public string Name
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NAME, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.NAME, value);
            }
        }
        public Color Color
        {
            get
            {
                if (XElement.TryGetAttribute(STRINGS.COLOR, null, out string colorString))
                    return new Color(colorString);

                return null;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.COLOR, value.ToString());
            }
        }
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Tile
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILE, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILE, value);
            }
        }
        public bool TileExists => XElement.TryGetAttribute(STRINGS.TILE, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Probability
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.PROBABILITY, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.PROBABILITY, value);
            }
        }
        public bool ProbabilityExists => XElement.TryGetAttribute(STRINGS.PROBABILITY, -1, out int _);
        #endregion

        #region Elements
        #endregion

        public EWangColor(XElement element) : base(element)
        {
        }
    }
}
