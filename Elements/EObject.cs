﻿using System;
using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EObject : ElementBase
    {
        #region Attributes
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Id
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.ID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.ID, value);
            }
        }
        public bool IdExists => XElement.TryGetAttribute(STRINGS.ID, -1, out int _);
        public string Name
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NAME, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.NAME, value);
            }
        }
        public string Type
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TYPE, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.TYPE, value);
            }
        }
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int X
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.X, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.X, value);
            }
        }
        public bool XExists => XElement.TryGetAttribute(STRINGS.X, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Y
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.Y, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.Y, value);
            }
        }
        public bool YExists => XElement.TryGetAttribute(STRINGS.Y, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Width
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.WIDTH, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.WIDTH, value);
            }
        }
        public bool WidthExists => XElement.TryGetAttribute(STRINGS.WIDTH, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Height
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.HEIGHT, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.HEIGHT, value);
            }
        }
        public bool HeightExists => XElement.TryGetAttribute(STRINGS.HEIGHT, -1, out int _);
        /// <summary>
        /// Defaults to 0
        /// </summary>
        public int Rotation
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.ROTATION, 0, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.ROTATION, value);
            }
        }
        public bool RotationExists => XElement.TryGetAttribute(STRINGS.ROTATION, -1, out int _);
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Gid
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.GID, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.GID, value);
            }
        }
        public bool GidExists => XElement.TryGetAttribute(STRINGS.GID, -1, out int _);
        public bool Visible
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.VISIBLE, false, out bool returnBool);
                return returnBool;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.VISIBLE, value);
            }
        }
        public bool VisibleExists
        {
            get
            {
                return XElement.TryGetAttribute(STRINGS.VISIBLE, false, out _);
            }
        }

        public string Template
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TEMPLATE, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.TEMPLATE, value);
            }
        }
        #endregion

        #region Elements
        public EProperties Properties
        {
            get
            {
                var properties = XElement.Element(STRINGS.PROPERTIES);
                return properties != null ? new EProperties(properties) : null;
            }
        }
        public EProperties CreateProperties()
        {
            return new EProperties(new XElement(STRINGS.PROPERTIES));
        }
        public void AddProperties(EProperties properties)
        {
            if (properties == null)
                throw new ArgumentNullException();

            if (Properties != null)
                throw new Exception($"Properties already exists on this Object.");

            XElement.Add(properties.XElement);
        }

        public EEllipse Ellipse
        {
            get
            {
                var ellipse = XElement.Element(STRINGS.ELLIPSE);
                return ellipse != null ? new EEllipse(ellipse) : null;
            }
        }
        public EEllipse CreateEllipse()
        {
            return new EEllipse(new XElement(STRINGS.ELLIPSE));
        }
        public void AddEllipse(EEllipse ellipse)
        {
            if (ellipse == null)
                throw new ArgumentNullException();

            if (Ellipse != null)
                throw new Exception($"Ellipse already exists on this Object.");

            XElement.Add(ellipse.XElement);
        }

        public EPoint Point
        {
            get
            {
                var point = XElement.Element(STRINGS.POINT);
                return point != null ? new EPoint(point) : null;
            }
        }
        public EPoint CreatePoint()
        {
            return new EPoint(new XElement(STRINGS.POINT));
        }
        public void AddPoint(EPoint point)
        {
            if (point == null)
                throw new ArgumentNullException();

            if (Point != null)
                throw new Exception($"Point already exists on this Object.");

            XElement.Add(point.XElement);
        }

        public EPolygon Polygon
        {
            get
            {
                var polygon = XElement.Element(STRINGS.POLYGON);
                return polygon != null ? new EPolygon(polygon) : null;
            }
        }
        public EPolygon CreatePolygon()
        {
            return new EPolygon(new XElement(STRINGS.POLYGON));
        }
        public void AddPolygon(EPolygon polygon)
        {
            if (polygon == null)
                throw new ArgumentNullException();

            if (Polygon != null)
                throw new Exception($"Polygon already exists on this Object.");

            XElement.Add(polygon.XElement);
        }

        public EPolyline Polyline
        {
            get
            {
                var polyline = XElement.Element(STRINGS.POLYLINE);
                return polyline != null ? new EPolyline(polyline) : null;
            }
        }
        public EPolyline CreatePolyline()
        {
            return new EPolyline(new XElement(STRINGS.POLYLINE));
        }
        public void AddPolyline(EPolyline polyline)
        {
            if (polyline == null)
                throw new ArgumentNullException();

            if (Polyline != null)
                throw new Exception($"Polyline already exists on this Object.");

            XElement.Add(polyline.XElement);
        }

        public EText Text
        {
            get
            {
                var text = XElement.Element(STRINGS.TEXT);
                return text != null ? new EText(text) : null;
            }
        }
        public EText CreateText()
        {
            return new EText(new XElement(STRINGS.TEXT));
        }
        public void AddText(EText text)
        {
            if (text == null)
                throw new ArgumentNullException();

            if (Text != null)
                throw new Exception($"Text already exists on this Object.");

            XElement.Add(text.XElement);
        }
        #endregion
        public EObject(XElement element) : base(element)
        {
        }
    }
}
