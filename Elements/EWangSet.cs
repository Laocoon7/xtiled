﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Citadel.XTiled.Tools;

namespace Citadel.XTiled.Elements
{
    public class EWangSet : ElementBase
    {
        #region Attributes
        public string Name
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.NAME, null, out string returnString);
                return returnString;
            }
            set
            {
                XElement.SetAttributeValue(STRINGS.NAME, value);
            }
        }
        /// <summary>
        /// Defaults to -1
        /// </summary>
        public int Tile
        {
            get
            {
                XElement.TryGetAttribute(STRINGS.TILE, -1, out int returnInt);
                return returnInt;
            }

            set
            {
                XElement.SetAttributeValue(STRINGS.TILE, value);
            }
        }
        public bool TileExists => XElement.TryGetAttribute(STRINGS.TILE, -1, out int _);
        #endregion

        #region Elements
        public EProperties Properties
        {
            get
            {
                var properties = XElement.Element(STRINGS.PROPERTIES);
                return properties != null ? new EProperties(properties) : null;
            }
        }
        public EProperties CreateProperties()
        {
            return new EProperties(new XElement(STRINGS.PROPERTIES));
        }
        public void AddProperties(EProperties properties)
        {
            if (properties == null)
                throw new ArgumentNullException();

            if (Properties != null)
                throw new Exception($"Properties already exists on this WangSet.");

            XElement.Add(properties.XElement);
        }

        public List<EWangColor> WangCornerColors
        {
            get
            {
                var returnValue = new List<EWangColor>();
                foreach (var wangColor in XElement.Elements(STRINGS.WANGCORNERCOLOR))
                {
                    returnValue.Add(new EWangColor(wangColor));
                }

                return returnValue;
            }
        }
        public EWangColor CreateWangCornerColor()
        {
            return new EWangColor(new XElement(STRINGS.WANGCORNERCOLOR));
        }
        public void AddWangCornerColor(EWangColor wangColor)
        {
            if (wangColor == null)
                throw new ArgumentNullException();

            var wangColors = WangCornerColors;
            if (wangColors.Count >= 15)
                throw new Exception($"WangSet already has 15 WangCornerColors.");

            if (wangColors.Count > 0)
                wangColors[wangColors.Count - 1].XElement.AddAfterSelf(wangColor.XElement);
            else
                XElement.Add(wangColor.XElement);
        }

        public List<EWangColor> WangEdgeColors
        {
            get
            {
                var returnValue = new List<EWangColor>();
                foreach (var wangColor in XElement.Elements(STRINGS.WANGEDGECOLOR))
                {
                    returnValue.Add(new EWangColor(wangColor));
                }

                return returnValue;
            }
        }
        public EWangColor CreateWangEdgeColor()
        {
            return new EWangColor(new XElement(STRINGS.WANGEDGECOLOR));
        }
        public void AddWangEdgeColor(EWangColor wangColor)
        {
            if (wangColor == null)
                throw new ArgumentNullException();

            var wangColors = WangEdgeColors;
            if (wangColors.Count >= 15)
                throw new Exception($"WangSet already has 15 WangEdgeColors.");

            if (wangColors.Count > 0)
                wangColors[wangColors.Count - 1].XElement.AddAfterSelf(wangColor.XElement);
            else
                XElement.Add(wangColor.XElement);
        }

        public List<EWangTile> WangTiles
        {
            get
            {
                var returnValue = new List<EWangTile>();
                foreach (var wangTile in XElement.Elements(STRINGS.WANGTILE))
                {
                    returnValue.Add(new EWangTile(wangTile));
                }

                return returnValue;
            }
        }
        public EWangTile CreateWangTile()
        {
            return new EWangTile(new XElement(STRINGS.WANGTILE));
        }
        public void AddWangTile(EWangTile wangTile)
        {
            if (wangTile == null)
                throw new ArgumentNullException();

            var wangTiles = WangTiles;
            if (wangTiles.Count > 0)
                wangTiles[wangTiles.Count - 1].XElement.AddAfterSelf(wangTile.XElement);
            else
                XElement.Add(wangTile.XElement);
        }
        #endregion
        public EWangSet(XElement element) : base(element)
        {
        }
    }
}
