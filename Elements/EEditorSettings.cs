﻿using System;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class EEditorSettings : ElementBase
    {
        #region Attributes
        #endregion

        #region Elements
        public EChunkSize ChunkSize
        {
            get
            {
                var chunkSize = XElement.Element(STRINGS.CHUNKSIZE);
                return chunkSize != null ? new EChunkSize(chunkSize) : null;
            }
        }
        public EChunkSize CreateChunkSize()
        {
            return new EChunkSize(new XElement(STRINGS.CHUNKSIZE));
        }
        public void AddChunkSize(EChunkSize chunkSize)
        {
            if (chunkSize == null)
                throw new ArgumentNullException();

            if (ChunkSize != null)
                throw new Exception($"ChunkSize already exists on this EditorSettings.");

            XElement.Add(chunkSize.XElement);
        }

        public EExport Export
        {
            get
            {
                var export = XElement.Element(STRINGS.EXPORT);
                return export != null ? new EExport(export) : null;
            }
        }
        public EExport CreateExport()
        {
            return new EExport(new XElement(STRINGS.EXPORT));
        }
        public void AddExport(EExport export)
        {
            if (export == null)
                throw new ArgumentNullException();

            if (Export != null)
                throw new Exception($"Export already exists on this EditorSettings.");

            XElement.Add(export.XElement);
        }
        #endregion

        public EEditorSettings(XElement element) : base(element)
        {
        }
    }
}
