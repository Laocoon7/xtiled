﻿using Citadel.XTiled.DataTypes;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class EPolygon : ElementBase
    {
        #region Attributes
        #endregion

        #region Elements
        #endregion

        #region Values
        public List<Point> Points
        {
            get
            {
                return Point.FromValue(XElement.Value);
            }
            set
            {
                XElement.SetValue(Point.ToValue(value));
            }
        }
        #endregion

        public EPolygon(XElement element) : base(element)
        {
        }
    }
}
