﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Citadel.XTiled.Elements
{
    public class ETerrainTypes : ElementBase
    {
        #region Attributes
        #endregion

        #region Elements
        public List<ETerrain> Terrains
        {
            get
            {
                var returnValue = new List<ETerrain>();
                foreach (var terrain in XElement.Elements(STRINGS.TERRAIN))
                {
                    returnValue.Add(new ETerrain(terrain));
                }

                return returnValue;
            }
        }
        public ETerrain CreateTerrain()
        {
            return new ETerrain(new XElement(STRINGS.TERRAIN));
        }
        public void AddTerrainType(ETerrain terrain)
        {
            if (terrain == null)
                throw new ArgumentNullException();

            var oldTerrains = Terrains;
            if (oldTerrains.Count > 0)
                oldTerrains[oldTerrains.Count - 1].XElement.AddAfterSelf(terrain.XElement);
            else
                XElement.Add(terrain.XElement);
        }
        #endregion

        public ETerrainTypes(XElement element) : base(element)
        {
        }
    }
}
