﻿# Changelog #
```
Restructured Project
XDocuments loaded from file now save the BaseUri.
int attributes now all have an (AttributeName)Exists function.
int attributes default return values have changed to be in line with actual defaults.
```