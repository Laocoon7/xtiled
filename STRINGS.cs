﻿namespace Citadel.XTiled
{
    public static class STRINGS
    {
        public const string MAX_VERSION = "1.4";

        public const string MAP = "map";
        public const string VERSION = "version";
        public const string TILEDVERSION = "tiledversion";
        public const string ORIENTATION = "orientation";
        public const string UNKNOWN = "unknown";
        public const string ORTHOGONAL = "orthogonal";
        public const string ISOMETRIC = "isometric";
        public const string STAGGERED = "staggered";
        public const string HEXAGONAL = "hexagonal";
        public const string RENDERORDER = "renderorder";
        public const string RIGHTDOWN = "right-down";
        public const string RIGHTUP = "right-up";
        public const string LEFTDOWN = "left-down";
        public const string LEFTUP = "left-up";
        public const string COMPRESSIONLEVEL = "compressionlevel";
        public const string WIDTH = "width";
        public const string HEIGHT = "height";
        public const string TILEWIDTH = "tilewidth";
        public const string TILEHEIGHT = "tileheight";
        public const string HEXSIDELENGTH = "hexsidelength";
        public const string STAGGERAXIS = "staggeraxis";
        public const string X = "x";
        public const string Y = "y";
        public const string STAGGERINDEX = "staggerindex";
        public const string EVEN = "even";
        public const string ODD = "odd";
        public const string BACKGROUNDCOLOR = "backgroundcolor";
        public const string NEXTLAYERID = "nextlayerid";
        public const string NEXTOBJECTID = "nextobjectid";
        public const string INFINITE = "infinite";

        public const string EDITORSETTINGS = "editorsettings";
        public const string CHUNKSIZE = "chunksize";
        public const string EXPORT = "export";
        public const string TARGET = "target";
        public const string FORMAT = "format";

        public const string TILESET = "tileset";
        public const string FIRSTGID = "firstgid";
        public const string SOURCE = "source";
        public const string NAME = "name";
        public const string SPACING = "spacing";
        public const string MARGIN = "margin";
        public const string TILECOUNT = "tilecount";
        public const string COLUMNS = "columns";
        public const string OBJECTALIGNMENT = "objectalignment";
        public const string UNSPECIFIED = "unspecified";
        public const string TOPLEFT = "topleft";
        public const string TOP = "top";
        public const string TOPRIGHT = "topright";
        public const string LEFT = "left";
        public const string CENTER = "center";
        public const string RIGHT = "right";
        public const string BOTTOMLEFT = "bottomleft";
        public const string BOTTOM = "bottom";
        public const string BOTTOMRIGHT = "bottomright";

        public const string IMAGE = "image";

        public const string TILEOFFSET = "tileoffset";

        public const string GRID = "grid";

        public const string TERRAINTYPES = "terraintypes";

        public const string TERRAIN = "terrain";

        public const string WANGSETS = "wangsets";

        public const string WANGSET = "wangset";

        public const string WANGCORNERCOLOR = "wangcornercolor";

        public const string WANGEDGECOLOR = "wangedgecolor";

        public const string WANGTILE = "wangtile";
        public const string TILEID = "tileid";
        public const string WANGID = "wangid";
        public const string HFLIP = "hflip";
        public const string VFLIP = "vflip";
        public const string DFLIP = "dflip";

        public const string TILE = "tile";
        public const string PROBABILITY = "probability";

        public const string ANIMATION = "animation";

        public const string FRAME = "frame";
        public const string DURATION = "duration";

        public const string LAYER = "layer";
        public const string ID = "id";
        public const string OPACITY = "opacity";
        public const string VISIBLE = "visible";
        public const string TINTCOLOR = "tintcolor";
        public const string OFFSETX = "offsetx";
        public const string OFFSETY = "offsety";

        public const string DATA = "data";
        public const string ENCODING = "encoding";
        public const string COMPRESSION = "compression";

        public const string CHUNK = "chunk";

        public const string GID = "gid";

        public const string OBJECTGROUP = "objectgroup";
        public const string DRAWORDER = "draworder";
        public const string INDEX = "index";
        public const string TOPDOWN = "topdown";

        public const string OBJECT = "object";
        public const string TYPE = "type";
        public const string ROTATION = "rotation";

        public const string TEMPLATE = "template";

        public const string ELLIPSE = "ellipse";

        public const string POINT = "point";

        public const string POLYGON = "polygon";

        public const string POLYLINE = "polyline";

        public const string TEXT = "text";
        public const string FONTFAMILY = "fontfamily";
        public const string PIXELSIZE = "pixelsize";
        public const string WRAP = "wrap";
        public const string COLOR = "color";
        public const string BOLD = "bold";
        public const string ITALIC = "italic";
        public const string UNDERLINE = "underline";
        public const string STRIKEOUT = "strikeout";
        public const string KERNING = "kerning";
        public const string HALIGN = "halign";
        public const string JUSTIFY = "justify";
        public const string VALIGN = "valign";

        public const string IMAGELAYER = "imagelayer";
        public const string TRANS = "trans";

        public const string GROUP = "group";

        public const string PROPERTIES = "properties";
        public const string PROPERTY = "property";
        public const string VALUE = "value";
        public const string STRING = "string";
        public const string INT = "int";
        public const string FLOAT = "float";
        public const string BOOL = "bool";
        public const string FILE = "file";

    }
}
