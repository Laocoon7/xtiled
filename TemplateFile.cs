﻿using Citadel.XTiled.Elements;
using System;
using System.Xml.Linq;

namespace Citadel.XTiled
{
    public class TemplateFile
    {
        XDocument XDocument;

        #region Elements
        public ETemplate Template
        {
            get
            {
                var template = XDocument.Element(STRINGS.TEMPLATE);
                return template != null ? new ETemplate(template) : null;
            }
        }
        public ETemplate CreateTemplate()
        {
            return new ETemplate(new XElement(STRINGS.TEMPLATE));
        }
        public void AddTemplate(ETemplate template)
        {
            if (template == null)
                throw new ArgumentNullException();

            if (Template != null)
                throw new Exception($"A Template already exists on this TemplateFile.");

            XDocument.Add(template.XElement);
        }
        #endregion

        public void Save(string filePath, bool disableFormatting = false)
        {
            if (disableFormatting)
                XDocument.Save(filePath, SaveOptions.DisableFormatting);
            else
                XDocument.Save(filePath);
        }

        public TemplateFile()
        {
            XDocument = new XDocument();
        }

        public TemplateFile(string templateFilePath)
        {
            XDocument = XDocument.Load(templateFilePath, LoadOptions.SetBaseUri);
        }

        public override string ToString()
        {
            return XDocument.ToString();
        }
    }
}
